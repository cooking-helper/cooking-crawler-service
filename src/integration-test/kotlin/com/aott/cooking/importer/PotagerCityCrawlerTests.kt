package com.aott.cooking.importer

import com.aott.cooking.importer.database.MongoConfig
import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Ingredient
import com.aott.cooking.importer.model.Recipe
import com.aott.cooking.importer.rest.controller.ImportRecipeController
import com.mongodb.MongoClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.litote.kmongo.KMongo
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = ["classpath:test.properties"])
class PotagerCityCrawlerTests {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    private val controller: ImportRecipeController? = null

    @Autowired
    private val mongoConfig: MongoConfig? = null

    private var mongoClient: MongoClient? = null

    @Before
    fun cleanDatabase() {
        if (mongoConfig != null) {
            mongoClient = KMongo.createClient(mongoConfig.getHost(), mongoConfig.getPort())
            mongoClient!!.getDatabase(mongoConfig.getDatabaseName()).getCollection<Recipe>().drop()
        } else {
            throw ExceptionInInitializerError()
        }
    }

    @Test
    fun contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    fun testPotagerCityImporter() {
        val urlToParse = "https://www.potagercity.fr/recette/crumble-de-rhubarbe-et-pommes/422"
        this.restTemplate?.put("http://localhost:$port/import", urlToParse)
        val importedRecipe = mongoClient!!.getDatabase("cooking").getCollection<Recipe>().findOne("{url:\"$urlToParse\"}");
        assertThat(importedRecipe?.title).isEqualTo("Crumble de rhubarbe et pommes")
        assertThat(importedRecipe?.id).isNotNull()
        assertThat(importedRecipe?.updateDate).isNotNull()
        assertThat(importedRecipe?.url).isEqualTo(urlToParse)
        assertThat(importedRecipe?.source).isEqualTo(CrawlerEnum.POTAGER_CITY)
        assertThat(importedRecipe?.totalTime).isEqualTo(40)
        assertThat(importedRecipe?.prepareTime).isEqualTo(10)
        assertThat(importedRecipe?.cookingTime).isEqualTo(30)
        assertThat(importedRecipe?.difficulty).isEqualTo(0)
        assertThat(importedRecipe?.cost).isEqualTo(0)
        assertThat(importedRecipe?.instructions).contains("Recette facile: la pomme adoucit l'acidité naturelle de la rhubarbe et les flocons d'avoine donne du croquant au crumble.")
        assertThat(importedRecipe?.instructions).contains("Laver, enlever les fils durs de la rhubarbe et la couper en morceaux.")
        assertThat(importedRecipe?.instructions).contains("Dans un saladier, mélanger les morceaux de rhubarbe et le sucre roux. Réserver au frais.")
        assertThat(importedRecipe?.instructions).contains("Éplucher, enlever le cœur et couper les pommes en morceaux.")
        assertThat(importedRecipe?.instructions).contains("Travailler rapidement avec les mains la farine, les flocons d'avoine, le sucre et le beurre froid coupé en morceaux pour créer une pâte sableuse.")
        assertThat(importedRecipe?.instructions).contains("Dans un moule, mélanger les pommes et la rhubarbe et couvrir les fruits avec la pâte sableuse. Si la rhubarbe a rendu de l'eau, l'égoutter.")
        assertThat(importedRecipe?.instructions).contains("Faire cuire au four à 180° pendant 30 minutes. Servir chaud ou tiède, accompagné de glace à la vanille.")
        assertThat(importedRecipe?.ingredients?.size).isEqualTo(7)
        assertThat(importedRecipe?.ingredients).contains(Ingredient("rhubarbe", 4, "tiges"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("farine", 125, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("flocons d'avoine", 60, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("sucre roux", 100, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("pommes", 2, ""))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("beurre", 125, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("sucre blanc", 125, "g"))
        assertThat(importedRecipe?.numberOfPiece).isEqualTo(3)
    }
}
