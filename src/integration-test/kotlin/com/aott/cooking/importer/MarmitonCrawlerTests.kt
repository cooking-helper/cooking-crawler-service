package com.aott.cooking.importer

import com.aott.cooking.importer.database.MongoConfig
import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Ingredient
import com.aott.cooking.importer.model.Recipe
import com.aott.cooking.importer.rest.controller.ImportRecipeController
import com.mongodb.MongoClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.litote.kmongo.KMongo
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = ["classpath:test.properties"])
class MarmitonCrawlerTests() {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

    @Autowired
    private val controller: ImportRecipeController? = null

    @Autowired
    private val mongoConfig: MongoConfig? = null

    private var mongoClient: MongoClient? = null

    @Before
    fun cleanDatabase() {
        if (mongoConfig != null) {
            mongoClient = KMongo.createClient(mongoConfig.getHost(), mongoConfig.getPort())
            mongoClient!!.getDatabase(mongoConfig.getDatabaseName()).getCollection<Recipe>().drop()
        } else {
            throw ExceptionInInitializerError()
        }
    }

    @Test
    fun contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    fun testMarmitonImporter() {
        val urlToParse = "http://www.marmiton.org/recettes/recette_tarte-aux-carottes-et-oignons_21826.aspx"
        this.restTemplate?.put("http://localhost:$port/import", urlToParse)
        val importedRecipe = mongoClient!!.getDatabase("cooking").getCollection<Recipe>().findOne("{url:\"$urlToParse\"}");
        assertThat(importedRecipe?.title).isEqualTo("Tarte aux carottes et oignons")
        assertThat(importedRecipe?.id).isNotNull()
        assertThat(importedRecipe?.updateDate).isNotNull()
        assertThat(importedRecipe?.url).isEqualTo(urlToParse)
        assertThat(importedRecipe?.source).isEqualTo(CrawlerEnum.MARMITON)
        assertThat(importedRecipe?.totalTime).isEqualTo(40)
        assertThat(importedRecipe?.prepareTime).isEqualTo(20)
        assertThat(importedRecipe?.cookingTime).isEqualTo(20)
        assertThat(importedRecipe?.difficulty).isEqualTo(1)
        assertThat(importedRecipe?.cost).isEqualTo(1)
        assertThat(importedRecipe?.instructions).contains("Peler les oignons, les couper en petits dés, et les faire fondre dans un peu d'huile.")
        assertThat(importedRecipe?.instructions).contains("Râper les carottes.")
        assertThat(importedRecipe?.instructions).contains("Battre la crème avec le carvi, et saler.")
        assertThat(importedRecipe?.instructions).contains("Rajouter l’œuf, et mélanger avec les carottes.")
        assertThat(importedRecipe?.instructions).contains("Taler la tarte dans un moule, et la piquer avec une fourchette.")
        assertThat(importedRecipe?.instructions).contains("Verser les oignons cuits sur la pâte, puis le mélange carottes - crème, et enfourner à th 6 (180°C), pendant environ 20 min.")
        assertThat(importedRecipe?.instructions).contains("Servir chaud ou froid.")
        assertThat(importedRecipe?.ingredients?.size).isEqualTo(8)
        assertThat(importedRecipe?.ingredients).contains(Ingredient("pâte brisée", 250, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("carotte", 400, "g"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("oignon", 3, ""))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("crème fraîche", 25, "cl"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("oeuf", 1, ""))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("carvi", 1, "cuillère à café"))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("sel", 0, ""))
        assertThat(importedRecipe?.ingredients).contains(Ingredient("poivre", 0, ""))
        assertThat(importedRecipe?.numberOfPiece).isEqualTo(8)
    }
}
