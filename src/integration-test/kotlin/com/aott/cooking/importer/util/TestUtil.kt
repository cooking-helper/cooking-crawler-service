package com.aott.cooking.importer.util

import com.aott.cooking.importer.rest.exception.ApiError
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.assertj.core.api.Assertions
import org.junit.Assert
import org.springframework.http.ResponseEntity


fun checkResponseStatus(response: ResponseEntity<String>, expectedStatus: Int) {
    Assertions.assertThat(response.statusCodeValue).isEqualTo(expectedStatus)
}

fun checkResponseNotEmpty(result: ResponseEntity<String>?) {
    Assert.assertNotNull(result)
}

fun checkErrorMessage(result: ResponseEntity<String>, expectedMessage: String) {
    val body: ApiError = getErrorFromResultBody(result.body)
    Assertions.assertThat(body.message).isEqualTo(expectedMessage)
}

fun getErrorFromResultBody(bodyResult: String?) : ApiError {
    val mapper = jacksonObjectMapper()
    return mapper.readValue(bodyResult, ApiError::class.java)
}