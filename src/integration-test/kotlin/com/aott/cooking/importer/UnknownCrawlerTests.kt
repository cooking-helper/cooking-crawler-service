package com.aott.cooking.importer

import com.aott.cooking.importer.database.MongoConfig
import com.aott.cooking.importer.model.Recipe
import com.aott.cooking.importer.rest.controller.ImportRecipeController
import com.aott.cooking.importer.util.checkErrorMessage
import com.aott.cooking.importer.util.checkResponseNotEmpty
import com.aott.cooking.importer.util.checkResponseStatus
import com.mongodb.MongoClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.litote.kmongo.KMongo.createClient
import org.litote.kmongo.getCollection
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.test.context.TestPropertySource



@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = ["classpath:test.properties"])
class UnknownCrawlerTests {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null

	@Autowired
	private val controller: ImportRecipeController? = null

    @Autowired
    private val mongoConfig: MongoConfig? = null

    private var mongoClient: MongoClient? = null

    @Before
    fun cleanDatabase() = if (this.mongoConfig != null) {
        mongoClient = createClient(this.mongoConfig.getHost(), this.mongoConfig.getPort())
        mongoClient!!.getDatabase(this.mongoConfig.getDatabaseName()).getCollection<Recipe>().drop()
    } else {
        throw ExceptionInInitializerError()
    }

	@Test
	fun contextLoads() {
		assertThat(controller).isNotNull;
	}

    @Test
    fun testUnknownImporter () {
        val urlToParse = "https://www.google.fr/recette/crumble-de-rhubarbe-et-pommes/422"
        val resourceUrl = "http://localhost:$port/import"
        val headers = HttpHeaders();
        val requestUpdate = HttpEntity(urlToParse, headers)
        val response = this.restTemplate?.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, String::class.java)
        checkResponseNotEmpty(response)
        checkResponseStatus(response!!, 501)
        checkErrorMessage(response, "Url came from a website that is not handled.")
    }
}