package com.aott.cooking.importer.database

import org.litote.kmongo.KMongo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MongoConnection @Autowired constructor(
        config: MongoConfig
) {
    private final val client = KMongo.createClient(config.getHost(), config.getPort())
    val database = client.getDatabase(config.getDatabaseName())!!
}