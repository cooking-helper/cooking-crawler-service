package com.aott.cooking.importer.database

import com.aott.cooking.importer.model.Recipe
import org.litote.kmongo.getCollection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class MongoRecipesQueries @Autowired constructor(
        private val mongo: MongoConnection
) {
    fun addRecipe(recipe: Recipe) {
        mongo.database.getCollection<Recipe>().insertOne(recipe)
    }
}