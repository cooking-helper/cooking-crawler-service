package com.aott.cooking.importer.rest.controller

import com.aott.cooking.importer.crawler.ICrawlerStrategy
import com.aott.cooking.importer.crawler.MarmitonCrawlerStrategy
import com.aott.cooking.importer.crawler.PotagerCityCrawlerStrategy
import com.aott.cooking.importer.database.MongoRecipesQueries
import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Recipe
import com.aott.cooking.importer.rest.exception.UnknownWebSiteException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/import")
class ImportRecipeController @Autowired constructor(
        private val recipesQueries: MongoRecipesQueries
) {
    @PutMapping(consumes = [MediaType.TEXT_PLAIN_VALUE])
    fun importRecipe(@RequestBody url: String) {
        val crawlerStrategy: ICrawlerStrategy = getStrategy(url)
        val extractedRecipe: Recipe = crawlerStrategy.extractRecipe(url)
        recipesQueries.addRecipe(extractedRecipe);
    }

    private fun getStrategy(url: String): ICrawlerStrategy {
        return when {
            url.contains(CrawlerEnum.MARMITON.id) -> MarmitonCrawlerStrategy()
            url.contains(CrawlerEnum.POTAGER_CITY.id) -> PotagerCityCrawlerStrategy()
            else -> throw UnknownWebSiteException("Url came from a website that is not handled.")
        }
    }
}