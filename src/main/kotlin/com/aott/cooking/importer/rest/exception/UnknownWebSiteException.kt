package com.aott.cooking.importer.rest.exception

class UnknownWebSiteException(message: String) : RuntimeException(message) {
}