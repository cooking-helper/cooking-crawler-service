package com.aott.cooking.importer.rest.exception

data class ApiError(val message: String?, val ex: Exception)