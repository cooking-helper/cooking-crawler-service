package com.aott.cooking.importer.crawler;

import java.util.regex.Pattern

fun extractIngredientName(ingredientString: String, ingredientUnit: String): String {
    val cleanFirst: String = ingredientString.removePrefix("$ingredientUnit de ")
    val cleanSecond: String = cleanFirst.removePrefix("$ingredientUnit d'")
    return cleanSecond.toLowerCase()
}

fun extractIngredientUnit(ingredientString: String): String {
    return when {
        ingredientString.startsWith("g ") -> "g"
        ingredientString.startsWith("kg ") -> "kg"
        ingredientString.startsWith("mg ") -> "mg"
        ingredientString.startsWith("cl ") -> "cl"
        ingredientString.startsWith("ml ") -> "ml"
        ingredientString.startsWith("dl ") -> "dl"
        ingredientString.startsWith("L ") -> "L"
        ingredientString.startsWith("cuillère à café ") -> "cuillère à café"
        ingredientString.startsWith("cuillère à soupe ") -> "cuillère à soupe"
        ingredientString.startsWith("tiges ") -> "tiges"
        else -> ""
    }
}

fun extractInt(str: String): Int {
    val matcher = Pattern.compile("\\d+").matcher(str)

    if (!matcher.find())
        return 0

    return Integer.parseInt(matcher.group())
}
