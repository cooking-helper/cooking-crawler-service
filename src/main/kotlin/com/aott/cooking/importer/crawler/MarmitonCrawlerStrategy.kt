package com.aott.cooking.importer.crawler

import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Ingredient
import org.jsoup.Jsoup
import org.jsoup.nodes.Document


class MarmitonCrawlerStrategy : ICrawlerStrategy {
    override fun getDocument(url: String): Document {
        return Jsoup.connect(url).get()
    }

    override fun extractInstructions(document: Document): String {
        val instructionsSteps = document.select(".recipe-preparation__list li")
        var instructions = ""
        for (instructionStep in instructionsSteps) {
            instructions += instructionStep.text().trim()
        }
        return instructions
    }

    override fun extractIngredients(document: Document): MutableList<Ingredient> {
        val ingredients: MutableList<Ingredient> = mutableListOf()
        val ingredientsElements = document.select(".recipe-ingredients__list li")
        for (ingredient in ingredientsElements) {
            val ingredientQuantityString: String = ingredient.select(".recipe-ingredient-qt").text()
            val ingredientQuantity: Int = extractInt(ingredientQuantityString)
            val ingredientString: String = ingredient.select(".ingredient").text()
            val ingredientUnit: String = extractIngredientUnit(ingredientString)
            val ingredientName: String = extractIngredientName(ingredientString, ingredientUnit)
            ingredients.add(Ingredient(ingredientName, ingredientQuantity, ingredientUnit))
        }
        return ingredients
    }

    override fun extractNumberOfPiece(document: Document): Int {
        val numberOfPieceString: String = document.select(".recipe-infos__quantity__value").text()
        return extractInt(numberOfPieceString)
    }

    override fun extractCost(document: Document): Int {
        val costString: String = document.select(".recipe-infos__budget > .recipe-infos__level__container").attr("class")
        return extractInt(costString)
    }

    override fun extractDifficulty(document: Document): Int {
        val difficultyString: String = document.select(".recipe-infos__level > .recipe-infos__level__container").attr("class")
        return extractInt(difficultyString)
    }

    override fun extractCookingTime(document: Document): Int {
        val cookingTimeString: String = document.select(".recipe-infos__timmings__cooking > .recipe-infos__timmings__value").text()
        return extractInt(cookingTimeString)
    }

    override fun extractPrepareTime(document: Document): Int {
        val prepareTimeString: String = document.select(".recipe-infos__timmings__preparation > .recipe-infos__timmings__value").text()
        return extractInt(prepareTimeString)

    }

    override fun extractTotalTime(document: Document): Int {
        val totalTimeString: String = document.select(".recipe-infos__timmings__total-time span").text()
        return extractInt(totalTimeString)
    }

    override fun extractTitle(document: Document) = document.select(".main-title").text().trim()

    override fun getSourceName() = CrawlerEnum.MARMITON
}