package com.aott.cooking.importer.crawler

import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Ingredient
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.openqa.selenium.chrome.ChromeDriver

class PotagerCityCrawlerStrategy : ICrawlerStrategy {
    override fun getDocument(url: String): Document {
        val driver = ChromeDriver()
        driver.get(url)
        val htmlContent = driver.pageSource
        driver.close()
        return Jsoup.parse(htmlContent)
    }

    override fun extractInstructions(document: Document): String {
        val instructionsSteps = document.select(".recette-detail__rollout > p")
        var instructions = ""
        for (instructionStep in instructionsSteps) {
            instructions += instructionStep.text().trim()
        }
        return instructions
    }

    override fun extractIngredients(document: Document): List<Ingredient> {
        val ingredients: MutableList<Ingredient> = mutableListOf()
        val ingredientsElements = document.select(".recette-detail__ingredients > p")
        for (ingredient in ingredientsElements) {
            val ingredientString: String = ingredient.text()
            val ingredientQuantity: Int = extractInt(ingredientString)
            val ingredientString2: String = ingredientString.replace(ingredientQuantity.toString(), "").trim()
            val ingredientUnit: String = extractIngredientUnit(ingredientString2)
            val ingredientName: String = extractIngredientName(ingredientString2, ingredientUnit)
            ingredients.add(Ingredient(ingredientName, ingredientQuantity, ingredientUnit))
        }
        return ingredients
    }

    override fun extractNumberOfPiece(document: Document): Int {
        val cookingTimeString: String = document.select(".preparation__item--cultery").text().trim()
        return extractInt(cookingTimeString)
    }

    override fun extractCost(document: Document): Int {
        //TODO("not implemented")
        return 0
    }

    override fun extractDifficulty(document: Document): Int {
        //TODO("not implemented")
        return 0
    }

    override fun extractCookingTime(document: Document): Int {
        val cookingTimeString: String = document.select(".preparation__item--saucepan").text().trim()
        return extractInt(cookingTimeString)
    }

    override fun extractPrepareTime(document: Document): Int {
        val prepareTimeString: String = document.select(".preparation__item--whisk").text().trim()
        return extractInt(prepareTimeString)
    }

    override fun getSourceName(): CrawlerEnum {
        return CrawlerEnum.POTAGER_CITY
    }

    override fun extractTotalTime(document: Document): Int {
        val cookingTime: Int = extractCookingTime(document)
        val prepareTime: Int = extractPrepareTime(document)
        return cookingTime + prepareTime
    }

    override fun extractTitle(document: Document) = document.select(".article-page__title").text().trim()
}