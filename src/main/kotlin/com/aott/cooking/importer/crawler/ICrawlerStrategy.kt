package com.aott.cooking.importer.crawler

import com.aott.cooking.importer.model.CrawlerEnum
import com.aott.cooking.importer.model.Ingredient
import com.aott.cooking.importer.model.Recipe
import org.jsoup.nodes.Document
import java.util.*

interface ICrawlerStrategy {
    fun extractRecipe(url: String): Recipe {
        val document = getDocument(url)
        val title: String = extractTitle(document)
        val totalTime = extractTotalTime(document)
        val source = getSourceName()
        val prepareTime = extractPrepareTime(document)
        val cookingTime = extractCookingTime(document)
        val difficulty: Int = extractDifficulty(document)
        val cost: Int = extractCost(document)
        val numberOfPiece: Int = extractNumberOfPiece(document)
        val ingredients: List<Ingredient> = extractIngredients(document)
        val instructions = extractInstructions(document)
        return Recipe(
                url = url,
                updateDate = Date(),
                cookingTime = cookingTime,
                difficulty = difficulty,
                numberOfPiece = numberOfPiece,
                cost = cost,
                prepareTime = prepareTime,
                source = source,
                title = title,
                totalTime = totalTime,
                instructions = instructions,
                ingredients = ingredients
        )
    }

    fun getDocument(url: String): Document

    fun extractInstructions(document: Document): String

    fun extractIngredients(document: Document): List<Ingredient>

    fun extractNumberOfPiece(document: Document): Int

    fun extractCost(document: Document): Int

    fun extractDifficulty(document: Document): Int

    fun extractCookingTime(document: Document): Int

    fun extractPrepareTime(document: Document): Int

    fun getSourceName(): CrawlerEnum

    fun extractTotalTime(document: Document): Int

    fun extractTitle(document: Document): String
}
