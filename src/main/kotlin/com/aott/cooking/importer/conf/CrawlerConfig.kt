package com.aott.cooking.importer.conf

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:config.properties")
@ConfigurationProperties(prefix = "crawler.config")
class CrawlerConfig {
    fun setChromeDriverPath(chromeDriverPathVal: String) {
        System.setProperty("webdriver.chrome.driver", chromeDriverPathVal)
    }
}