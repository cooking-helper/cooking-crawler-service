package com.aott.cooking.importer.model

enum class CrawlerEnum(val id: String) {
    NONE("none"), MARMITON("marmiton"), POTAGER_CITY("potagercity")
}