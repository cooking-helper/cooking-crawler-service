package com.aott.cooking.importer.model

import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId
import java.util.*

data class Recipe(
        @BsonId val id: ObjectId? = null, val updateDate: Date, val url: String, val source: CrawlerEnum = CrawlerEnum.NONE, val title: String = "", val totalTime: Int = 0, val prepareTime: Int = 0, val cookingTime: Int = 0, val difficulty: Int = 0, val cost: Int = 0, val numberOfPiece: Int = 0, val ingredients: List<Ingredient> = ArrayList(), val instructions: String = "")
