package com.aott.cooking.importer.model

data class Ingredient(val name: String, val quantity: Int, val unit: String)
